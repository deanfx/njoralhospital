// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Mint from 'mint-ui'
// import store from './vuex/'
import 'mint-ui/lib/style.css'
import './style/iconfont.css'
import './style/style.css'
import './style/iconfont'
import { Toast } from 'mint-ui';

Vue.use(Mint);

Vue.config.productionTip = false;
Vue.prototype.$cmfdone = function(txt){
  Toast({
    message: txt,
    iconClass: 'iconfont icon-msnui-success'
  });
  setTimeout(function(){
    router.push('/')
  },2500)
};



/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// });
// document.addEventListener('deviceready', function() {
  new Vue({
    el: '#app',
    router,
    // store,
    template: '<App/>',
    components: { App }
  });
  // window.navigator.splashscreen.hide()
// }, false);
