import Vue from 'vue'
import Router from 'vue-router'
import Mainpage from '@/pages/Mainpage'
import Search from '@/pages/Search'
import Map from '@/pages/Map'
import Doctor from '@/pages/Doctor'
import Detail from '@/pages/Detail'
import DoctorDetail from '@/pages/DoctorDetail'
import Result from '@/pages/Result'
import ResultDetail from '@/pages/ResultDetail'
import Guide from '@/pages/Guide'
import Pay from '@/pages/Pay'
import Payment from '@/pages/Payment'
import Paylist from '@/pages/Paylist'
import Readd from '@/pages/Readd'
import Regpre from '@/pages/Regpre'
import Regtdy from '@/pages/Regtdy'
import Regpay from '@/pages/Regpay'
import Newacct from '@/pages/Newacct'
import Newcard from '@/pages/Newcard'
import Myfav from '@/pages/Myfav'
import Myreg from '@/pages/Myreg'
import Myrest from '@/pages/Myrest'
import Mysick from '@/pages/Mysick'
import Myinfo from '@/pages/Myinfo'
import Feedback from '@/pages/Feedback'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Mainpage',
      component: Mainpage
    },
    {
      path: '/Search',
      name: 'Search',
      component: Search
    },
    {
      path: '/Map',
      name: 'Map',
      component: Map
    },
    {
      path: '/Doctor',
      name: 'Doctor',
      component: Doctor
    },
    {
      path: '/Detail',
      name: 'Detail',
      component: Detail
    },
    {
      path: '/DoctorDetail',
      name: 'DoctorDetail',
      component: DoctorDetail
    },
    {
      path: '/Result',
      name: 'Result',
      component: Result
    },
    {
      path: '/ResultDetail',
      name: 'ResultDetail',
      component: ResultDetail
    },
    {
      path: '/Guide',
      name: 'Guide',
      component: Guide
    },
    {
      path: '/Pay',
      name: 'Pay',
      component: Pay
    },
    {
      path: '/Payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/Paylist',
      name: 'Paylist',
      component: Paylist
    },
    {
      path: '/Readd',
      name: 'Readd',
      component: Readd
    },
    {
      path: '/Regpre',
      name: 'Regpre',
      component: Regpre
    },
    {
      path: '/Regtdy',
      name: 'Regtdy',
      component: Regtdy
    },
    {
      path: '/Regpay',
      name: 'Regpay',
      component: Regpay
    },
    {
      path: '/Newacct',
      name: 'Newacct',
      component: Newacct
    },
    {
      path: '/Newcard',
      name: 'Newcard',
      component: Newcard
    },
    {
      path: '/Myfav',
      name: 'Myfav',
      component: Myfav
    },
    {
      path: '/Myrest',
      name: 'Myrest',
      component: Myrest
    },
    {
      path: '/Myreg',
      name: 'Myreg',
      component: Myreg
    },
    {
      path: '/Mysick',
      name: 'Mysick',
      component: Mysick
    },
    {
      path: '/Myinfo',
      name: 'Myinfo',
      component: Myinfo
    },
    {
      path: '/Feedback',
      name: 'Feedback',
      component: Feedback
    }
  ]
})
